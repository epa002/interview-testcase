import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css'],
    providers: [UserService]
})
export class UserComponent implements OnInit {
    private users: Object;



    constructor(private userService: UserService) { }

    getUsers() {
        this.userService.get()
            .subscribe(
            users => this.users = users);
        console.log(this.users);
        }

    ngOnInit() {
        this.getUsers();
    }

    private newUser = {
        name: '',
        email: '',
    };
    addUser() {
        return this.userService.add(this.newUser)
    }

}

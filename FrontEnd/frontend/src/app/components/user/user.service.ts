import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private options = new RequestOptions({ headers: this.headers });

    constructor(private http: Http) { }

    get() {
        return this.http.get('http://localhost:53103/api/user/get')
            .map(r => r.json());
                }

    private response;

    add(user) {
        console.log(user);
        let encapsuled = "'" + JSON.stringify(user) + "'";
        return this.http.post('http://localhost/api/user/insert', encapsuled, this.options)
}
}
import { Component, OnInit } from '@angular/core';
import { TodoService } from './todo.service';
@Component({
    selector: 'app-todo',
    templateUrl: './todo.component.html',
    providers: [TodoService]
})
export class TodoComponent implements OnInit {
    private todos;
    private newTodo = {
        category: '',
        text:'',
    }
    constructor(private todoService: TodoService) { }
    getTodos() {
        return this.todoService.get().then(todos => {
            this.todos = todos;
        });
    }

    ngOnInit() {
        this.getTodos();
    }

    addTodo() {
        this.todoService.add({ category: this.newTodo.category, text: this.newTodo.text })
            .then(() => { return this.getTodos();
            }).then(() => {
                this.newTodo.category = '';
                this.newTodo.text = '';
            });
    }
}
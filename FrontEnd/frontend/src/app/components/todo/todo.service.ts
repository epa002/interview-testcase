import { Injectable } from '@angular/core';
let todos = [
    { category: 'Setup API', text: 'Dette m� gj�res' },
];
@Injectable()
export class TodoService {
    constructor() { }
    get() {
        return new Promise(resolve => resolve(todos));
    }

    add(data) {
        return new Promise(resolve => {
            todos.push(data);
            resolve(data);
        });
    }
}